FROM python:slim-buster

RUN python3 -m venv /opt/venv

#install requirements
COPY requirements.txt .
RUN . /opt/venv/bin/activate && pip install -r requirements.txt

COPY . .

CMD . /opt/venv/bin/activate && exec python app/main.py
