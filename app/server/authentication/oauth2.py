from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from . import jwt_token
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from . import jwt_token
from jose import JWTError, jwt
from decouple import config

from ..database import (
    get_specific_user,
    get_specific_user_by_email
)

from ..models.token_model import(
    Token,
    TokenData
)


SECRET_KEY = config('JWT_SECRET_KEY')
ALGORITHM = config('ALGORITHM')
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")


async def get_current_user(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        email: str = payload.get("sub")
        if email is None:
            raise credentials_exception
        token_data = TokenData(email=email)
    except JWTError:
        raise credentials_exception
    user = await get_specific_user_by_email(token_data.email)
    if user is None:
        raise credentials_exception
    return user
