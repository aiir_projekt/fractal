import motor.motor_asyncio
from bson.objectid import ObjectId
from decouple import config

MONGO_DETAILS = config('MONGO_DETAILS')
client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)
database = client.users
user_collection = database.get_collection("users_collection")


def user_helper(user) -> dict:
    return{
        "id": str(user["_id"]),
        "fullname": str(user["fullname"]),
        "email": str(user["email"]),
        "password": str(user["password"]),
    }


async def get_users():
    users = []
    async for user in user_collection.find():
        users.append(user_helper(user))
    return users

async def get_specific_user(id: str) -> dict:
    user = await user_collection.find_one({"_id": ObjectId(id)})
    if user:
        return user_helper(user)

async def get_specific_user_by_email(email: str) -> dict:
    user = await user_collection.find_one({"email": email})
    if user:
        return user_helper(user)


async def add_user(user_data: dict) -> dict:
    user = await user_collection.insert_one(user_data)
    new_user = await user_collection.find_one({"_id": user.inserted_id})
    return user_helper(new_user)


async def delete_user(id: str):
    user = await user_collection.find_one({"_id": ObjectId(id)})
    print(user)
    if user != None:
        await user_collection.delete_one({"_id": ObjectId(id)})
        return True

async def update_user(id: str, data: dict):
    # False if passed data is empty
    if len(data) < 1: 
        return False
    user = await user_collection.find_one({"_id": ObjectId(id)})


    if user:
        updated_user = await user_collection.update_one(
            {"_id": ObjectId(id)}, {"$set": data}
        )
        
        if updated_user:
            return True
        return False


#############################################
# LOGIN

async def check_if_email_exists(email):
    user = await user_collection.find_one({"email": email})
    if user:
        return True
    return False


async def check_login_creds(email: str) -> dict: 
    user = await user_collection.find_one({"email": email})
    if user:
        return user_helper(user)
    return False
    
