import random
from typing import List

from fastapi import APIRouter, WebSocket,WebSocketDisconnect, Body, HTTPException, status, Depends, Response
from fastapi.responses import HTMLResponse
import base64
from PIL import Image
import json
import pika
import uuid


router = APIRouter()

class ConnectionManager:
    def __init__(self):
        self.active_connections: List[WebSocket] = []
        self.fractal_id = 0

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.active_connections.append(websocket)

    def disconnect(self, websocket: WebSocket):
        self.active_connections.remove(websocket)

    async def send_personal_message(self, message: str, websocket: WebSocket):
        await websocket.send_text(message)

    async def broadcast(self, message: str):
        for connection in self.active_connections:
            await connection.send_text(message)


manager = ConnectionManager()


@router.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await manager.connect(websocket)
    try:
        while True:
            request = await websocket.receive_json()
            manager.fractal_id = str(uuid.uuid4())
            request['id'] = manager.fractal_id
            request = json.dumps(request)

            connection = pika.BlockingConnection(
                pika.ConnectionParameters('rabbitmq'))
            channel = connection.channel()
            channel.queue_declare(queue='queue')
            channel.basic_publish(exchange='',
                                  routing_key='queue',
                                  body=bytes(request, 'ascii'))
            channel.queue_declare(str(manager.fractal_id))
            for method, properties, body in channel.consume(str(manager.fractal_id)):
                channel.basic_ack(method.delivery_tag)
                await websocket.send_json(body.decode())
                channel.cancel()

    except WebSocketDisconnect:
        manager.disconnect(websocket)
        await manager.broadcast(f"Disconnect")
