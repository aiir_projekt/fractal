from fastapi import APIRouter, Body,HTTPException, status, Depends
from fastapi.encoders import jsonable_encoder

from ..database import (
    get_users,
    get_specific_user,
    add_user,
    delete_user,
    update_user,
    user_helper,
    check_if_email_exists
    )

from ..models.user_model import (
    UserSchema,
    ResponseModel,
    ErrorResponseModel,
    UpdateUserSchema,
)

from ..authentication.oauth2 import (
    get_current_user,

)

from ..authentication.oauth2 import get_current_user
from ..authentication import hashing

router = APIRouter()

@router.post("/register", response_description="User data added into DB")
async def add_user_data(user: UserSchema = Body(...)):
    user = jsonable_encoder(user)
    new_user = await check_if_email_exists(user['email'])
    if new_user:
        raise HTTPException(
            status_code=409,
            detail=f"Inserted email is already in use"
        )
    else:
        user["password"] = hashing.get_password_hash(user["password"])
        new_user = await add_user(user)
        if new_user:
            return ResponseModel(new_user, "User added successfully")
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Error during adding user"
            )


@router.get("/all", response_description="Users data retrived")
async def get_all_users(current_user: UserSchema = Depends(get_current_user)):
    users = await get_users()
    if users:
        return ResponseModel(users, "Users data retrived successfully")
    return ResponseModel(users, "Empty list returned")


@router.get("/me", response_description="User data retrived")
async def get_user_data(current_user: UserSchema = Depends(get_current_user)):
    if current_user:
        return ResponseModel(current_user, "Specific user retrived successfully")
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"User doesn't exist"
        )


@router.delete("/{id}", response_description="User data deleted from DB")
async def delete_user_data(id, current_user: UserSchema = Depends(get_current_user)):
    deleted_user = await delete_user(id)
    if delete_user:
        return ResponseModel("User with ID: {} removed".format(id), "Student deleted successfully")
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"User with this id doesn't exist"
        )


@router.put("/{id}", response_description="User data successfully updated")
async def update_user_data(id, req: UpdateUserSchema = Body(...),current_user: UserSchema = Depends(get_current_user)):
    req = {k: v for k, v in req.dict().items() if v is not None}
    new_user = await update_user(id, req)
    if new_user:
        return ResponseModel("User with ID: {} updated".format(id), "Student updated successfully")
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Error during user update"
        )
