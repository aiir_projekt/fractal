from fastapi import APIRouter, Body, HTTPException, status, Depends
from fastapi.encoders import jsonable_encoder
from fastapi.security import OAuth2PasswordRequestForm
from ..authentication import jwt_token, hashing
from decouple import config

SECRET_KEY = config('JWT_SECRET_KEY')


from ..models.user_model import (
    UserSchema,
    LoginSchema,
    ResponseModel,
    ErrorResponseModel,
    UpdateUserSchema,
    
)


from ..database import (
    check_login_creds
)


router = APIRouter()

@router.post('/', response_description="Login")
async def login(request: OAuth2PasswordRequestForm = Depends() ):
    new_user = jsonable_encoder(request)
    logged_user = await check_login_creds(request.username)
    if logged_user:
        logged_user = jsonable_encoder(logged_user)
        #if logged_user['password'] == new_user['password']:
        if hashing.verify_password( request.password, logged_user['password']):
            #access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
            access_token = jwt_token.create_access_token(
                data={"sub": request.username}
            )
            return {"access_token": access_token, "token_type": "bearer"}

        else:
            raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Invalid password")

    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Invalid Credentials")



