from typing import Optional
from pydantic import BaseModel, EmailStr, Field

class UserSchema(BaseModel):
    fullname: str = Field(...)
    email: EmailStr = Field(...)
    password: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "fullname": "Bob Marley",
                "email": "marleyB420@o2.com",
                "password": "pass"
            }
        }


class UpdateUserSchema(BaseModel):
    fullname: Optional[str]
    email: Optional[EmailStr]
    password: Optional[str]


    class Config:
        schema_extra = {
            "example": {
                "fullname": "Bob Marley",
                "email": "marleyB420@o2.com",
                "password": "pass"
            }
        }


class LoginSchema(BaseModel):
    email: str = Field(...)
    password: str = Field(...)


def ResponseModel(data, message):
    return {
        "data": [data],
        "code": 200,
        "message": message,
    }


def ErrorResponseModel(error, code, message):
    return {"error": error, "code": code, "message": message}
