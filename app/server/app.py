from fastapi import FastAPI ,WebSocket,WebSocketDisconnect
from fastapi.responses import HTMLResponse

from fastapi.middleware.cors import CORSMiddleware
from server.routes.user_routes import router as UserRouter
from server.routes.authentication_routes import router as AuthenticationRouter
from server.routes.fractal_generator_routes import router as FractalGeneratorRouter

origins = ["http://localhost:4200", "https://aiir.mzadka.me"]

app = FastAPI()
app.add_middleware(CORSMiddleware, allow_origins=origins,
                   allow_methods=["*"], allow_headers=["Authorization"])

app.include_router(UserRouter, tags=["User"], prefix="/user")
app.include_router(AuthenticationRouter, tags=[
                   "Authentication"], prefix="/login")
app.include_router(FractalGeneratorRouter, tags=[
                   "Fractal Generator"], prefix="/fractal_generator")


@app.get("/", tags=["Root"])
async def read_root():
    return {"message": "HELLO!"}


