import uvicorn
from decouple import config

port = config('PORT', cast=int, default=8000)

if __name__ == "__main__":
    uvicorn.run("server.app:app", port=port, host="0.0.0.0", reload=False)
