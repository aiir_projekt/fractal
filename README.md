<!-- @format -->

**Usage**

Create and run python virtual environment in fractal catalog

`python3 -m venv venv`

`source venv/bin/activate`

Install requirments using command below

`pip3 install -r requirements.txt `

Start server from fractal/app catalog

`python3 app/main.py`

Server works on `localhost:8000`
,to run Swagger UI and check documentation with HTTP requests go to `localhost:8000/docs`
